﻿
using extension_sdk;
using price_agreement.agreement;
using price_agreement.basic;
using pricing_engine.data_model;
using System;
using System.Linq;

namespace pricing_engine.service
{
    public class ValidationManager
    {
        public virtual bool IsValidPriceRule(IProduct product, IListPriceRule listPriceRule, DateTime pricingDate)
        {
            return IsProductValid(product, listPriceRule.AgreementProduct)
                && IsPricingDateValid(pricingDate, listPriceRule.StartDate, listPriceRule.EndDate);

        }

        public virtual bool IsValidDiscountRule(AbstractSettledOrderLine upsertedSettledLine, IDiscountRule discountRule, DateTime pricingDate)
        {
            return IsProductValid(upsertedSettledLine.Product, discountRule.AgreementProduct)
                && IsPricingDateValid(pricingDate, discountRule.StartDate, discountRule.EndDate)
                && IsTriggerConditionMet(upsertedSettledLine, discountRule);

        }

        public virtual bool IsProductValid(IProduct product, IAgreementProduct agrementProduct)
        {
            return agrementProduct.ResolvedProductList.Any(p => p.Id.Equals(product.Id));
        }

        public virtual bool IsPricingDateValid(DateTime pricingDate, DateTime? startDate, DateTime? endDate)
        {
            return pricingDate.IsBetween(startDate, endDate);
        }

        //A default implementation to trigger the rule Buy X unit for Y dollars off 
        public virtual bool IsTriggerConditionMet(AbstractSettledOrderLine settledLine, IDiscountRule agreementRule)
        {
            //Where there's no remainder on settled quantity, valid to trigger the rule
            return settledLine.GetTotalSettledQuantity() % agreementRule.TriggerQuantity == 0;
        }
    }
}
