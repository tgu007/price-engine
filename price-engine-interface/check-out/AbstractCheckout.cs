﻿using extension_sdk;
using price_agreement.agreement;
using price_agreement.basic;
using pricing_engine.data_model;
using pricing_engine.exception;
using pricing_engine.service.strategy;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace pricing_engine.service
{
    public abstract class AbstractCheckout
    {
        private List<IPriceAgreement> agreementList;
        private SortedList<int, ICalculateDiscountStrategy> calculationStrategyList;
        private ValidationManager validationManager;
        private AbstractSettledOrder settlementOrder;


        //Default constructor
        public AbstractCheckout(List<IPriceAgreement> agreementList)
        {
            if (agreementList == null)
                throw new PricingEngineException(ConfigurationManager.AppSettings["exAgreementNotInitialized"]);


            this.agreementList = agreementList;

            //Use defualt validation manger if not passed in
            validationManager = new ValidationManager();

            //Use defualt strategy manger if not passed in
            calculationStrategyList = new SortedList<int, ICalculateDiscountStrategy>() { { 1, new StrategyManager() } };

            //Create SettlmentOrder
            settlementOrder = this.NewSettlementOrder();
        }

        //Constructor to allow custom validation
        public AbstractCheckout(List<IPriceAgreement> agreementList, ValidationManager validationManager)
            : this(agreementList)
        {
            if (validationManager != null)
                this.validationManager = validationManager;
        }

        //Constructor to allow custom calculation Strategy
        public AbstractCheckout(List<IPriceAgreement> agreementList, ValidationManager validationManager, SortedList<int, ICalculateDiscountStrategy> calculationStrategyList)
            : this(agreementList, validationManager)
        {
            if (calculationStrategyList != null)
                this.calculationStrategyList = calculationStrategyList;
        }

        public abstract AbstractSettledOrder NewSettlementOrder();

        //Mark virtul to support completely different implementation
        public virtual void Scan(IOrderLine orderLine)
        {
            try
            {
                IListPriceRule listPriceRule = this.FindListPriceRule(orderLine.Product, PricingDate);
                //Create or updated the existing settled line.
                AbstractSettledOrderLine upsertedSettledLine = SettledOrder.UpsertSettledLine(orderLine, listPriceRule);
               
                //Try find if there's any valid discount rule.
                IDiscountRule discountRule = this.FindDiscountRule(upsertedSettledLine);

                if (discountRule != null)
                {
                    //Set the discount rule on line detail than do calculation
                    upsertedSettledLine.GetLastLineDetail().DiscountRuleApplied = discountRule;
                    SetDiscountOnSettledLine(upsertedSettledLine);
                }
            }
            catch (Exception ex)
            {
                throw new PricingEngineException(ConfigurationManager.AppSettings["exDuringScan"], ex);
            }
        }



        //Find list price rule for scanned item.
        protected virtual IListPriceRule FindListPriceRule(IProduct product, DateTime pricingDate)
        {
            List<IListPriceRule> allValidListPriceRule = agreementList
                    .SelectMany(agreement => agreement.AgreementListPriceRuleList)
                    .Where(rule => validationManager.IsValidPriceRule(product, rule, PricingDate))
                    .ToList();

            if (allValidListPriceRule.Count > 0)
            {
                if (allValidListPriceRule.Count > 1)
                    throw new NotImplementedException();

                return allValidListPriceRule[0];
            }
            else
                throw new PricingEngineException("List price rule not found");

        }

        //Find discount rule for scanned item.
        protected virtual IDiscountRule FindDiscountRule(AbstractSettledOrderLine upsertedSettledLine)
        {
            List<IDiscountRule> allValidDiscountRule = agreementList
                     .SelectMany(agreement => agreement.AgreementDiscountRuleList)
                     .Where(rule => validationManager.IsValidDiscountRule(upsertedSettledLine, rule, PricingDate))
                     .ToList();

            if (allValidDiscountRule.Count > 0)
            {
                if (allValidDiscountRule.Count > 1)
                    throw new NotImplementedException();

                return allValidDiscountRule[0];
            }
            else
            {
                return null;
            }
        }


        //Method to calculate the discount using strategy pattern, provide a default if no Strategy patten passed in.
        //Mark virtual to allow override and customized implementation
        protected virtual void SetDiscountOnSettledLine(AbstractSettledOrderLine upsertedSettledLine)
        {
            //Looping throw the passed in strategy and execute them by order
            foreach (var strategyPair in calculationStrategyList)
                strategyPair.Value.PerformCalculation(upsertedSettledLine);
        }

        //Add a pricing date to support backward time pricing, default to now
        protected DateTime PricingDate { get => DateTime.Now; }

        //Data model to store the result. 
        public AbstractSettledOrder SettledOrder { get => this.settlementOrder; }

        public SortedList<int, ICalculateDiscountStrategy> CalculationStrategyList { set { this.calculationStrategyList = value; }}

        public ValidationManager ValidationManager { set { this.validationManager = value; } }
    }
}
