﻿using price_agreement.agreement;
using pricing_engine.data_model;
using pricing_engine.exception;
using System;
using System.Configuration;
using System.Linq;

namespace pricing_engine.service.strategy
{
    public interface ICalculateDiscountStrategy
    {
        //Implement a default strategy here
        //eg buy 2 get 10 dollars off on total price
        public void PerformCalculation(AbstractSettledOrderLine settledOrderLine)
        {
            AbstractSettledLineDetail lineDetail = settledOrderLine.GetLastLineDetail();
            IDiscountRule agreementRule = lineDetail.DiscountRuleApplied;
            lineDetail.AppliedDiscount = Math.Round(agreementRule.DiscountValue, 2);  
        }

       
    }
}
