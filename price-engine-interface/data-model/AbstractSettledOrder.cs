﻿using price_agreement.agreement;
using price_agreement.basic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace pricing_engine.data_model
{
    public abstract class AbstractSettledOrder
    {
        private List<AbstractSettledOrderLine> settledLines;

        public AbstractSettledOrder()
        {
            settledLines = new List<AbstractSettledOrderLine>();
        }

        public virtual AbstractSettledOrderLine UpsertSettledLine(IOrderLine orderLine, IListPriceRule listPriceRule)
        {

            //Try find if this product already been scaned before.
            AbstractSettledOrderLine settledOrderLine = SettledLines.SingleOrDefault(sl => sl.Product.Id.Equals(orderLine.Product.Id) && sl.ListPriceRuleApplied == listPriceRule);
            if (settledOrderLine == null)
            {
                settledOrderLine = NewLine(orderLine.Product, listPriceRule);
                settledLines.Add(settledOrderLine);
            }
            
            settledOrderLine.AddSettledLineDetail(orderLine);
           
            return settledOrderLine;
        }

        public abstract AbstractSettledOrderLine NewLine(IProduct product, IListPriceRule listPriceRule);

        public virtual List<AbstractSettledOrderLine> SettledLines { get => settledLines; }

        public virtual decimal getTotalPrice ()
        {
            return settledLines.Sum(line => line.GetTotalPriceAfterDiscount());
        }

    }
}
