﻿
using price_agreement.agreement;
using price_agreement.basic;
using System;
using System.ComponentModel.DataAnnotations;

namespace pricing_engine.data_model
{
    public interface IOrderLine
    {
        [Range(0.01, double.MaxValue, ErrorMessage = "Quantity must greater than 0")]
        public decimal Quantity { get => 1; }
        public IProduct Product { get; }

    }
}
