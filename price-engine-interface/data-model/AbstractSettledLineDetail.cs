﻿

using price_agreement.agreement;
using System;

namespace pricing_engine.data_model
{
    public abstract class AbstractSettledLineDetail 
    {
        //Todo  store the strategy list applied
        public AbstractSettledLineDetail(IOrderLine scanedLineSnapshot)
        {
            ScanedLineSnapshot = scanedLineSnapshot;
        }

        public virtual IOrderLine ScanedLineSnapshot { get; set; }

        public virtual IDiscountRule DiscountRuleApplied { get; set; }

        

        public virtual decimal AppliedDiscount { get; set; }

       

        public virtual decimal GetTotalDiscount()
        {
            if (AppliedDiscount == 0)
                return 0;
            return Math.Round(AppliedDiscount, 2);
        }

    }
}
