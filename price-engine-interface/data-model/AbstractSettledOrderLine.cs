﻿using price_agreement.agreement;
using price_agreement.basic;
using pricing_engine.exception;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace pricing_engine.data_model
{
    public abstract class AbstractSettledOrderLine
    {
        public AbstractSettledOrderLine(IProduct product, IListPriceRule listPriceRule)
        {
            Product = product;
            LineDetailList = new SortedList<int, AbstractSettledLineDetail>();
            ListPriceRuleApplied = listPriceRule;
        }
        internal void AddSettledLineDetail(IOrderLine orderLine)
        {
            AbstractSettledLineDetail newOriginOrderLine = this.NewLineDetail(orderLine);
            this.LineDetailList.Add(LineDetailList.Count + 1, newOriginOrderLine);
            
        }

        public abstract AbstractSettledLineDetail NewLineDetail(IOrderLine orderLine);

        public virtual AbstractSettledLineDetail GetLastLineDetail()
        {
            AbstractSettledLineDetail lastLineDetail = this.LineDetailList.Values.LastOrDefault();
            if (lastLineDetail == null)
                throw new PricingEngineException(ConfigurationManager.AppSettings["exLastScanedItemNotFound"]);
            return lastLineDetail;
        }

        public virtual decimal GetTotalSettledQuantity()
        {
            return LineDetailList.Values.Sum(lineDetail => lineDetail.ScanedLineSnapshot.Quantity);
        }

        public virtual decimal GetTotalPriceBeforeDiscount()
        {
            return Math.Round(ListPriceRuleApplied.ListPrice * GetTotalSettledQuantity(), 2);
        }

        public virtual decimal GetTotalDiscountApplied()
        {
            return LineDetailList.Values.Sum(lineDetail => lineDetail.GetTotalDiscount());
        }

        public virtual decimal GetTotalPriceAfterDiscount()
        {
            return this.GetTotalPriceBeforeDiscount() - this.GetTotalDiscountApplied();
        }

        public virtual SortedList<int, AbstractSettledLineDetail> LineDetailList { get; set; }

        public virtual IProduct Product { get; set; }

        public virtual IListPriceRule ListPriceRuleApplied { get; set; }


    }
}
