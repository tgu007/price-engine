﻿using System;
using System.Configuration;

namespace pricing_engine.exception
{
    public class PricingEngineException : Exception
    {
        public PricingEngineException() { }

        public PricingEngineException(string message)
        : base(string.Format(ConfigurationManager.AppSettings["exDuringPricing"], message))
        {

        }

        public PricingEngineException(string message, Exception innerException)
       : base(string.Format(ConfigurationManager.AppSettings["exDuringPricing"]), innerException)
        {

        }
    }
}



