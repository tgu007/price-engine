﻿
using System.Configuration;

namespace pricing_engine.exception
{
    public class InvalidSettledPriceException: PricingEngineException
    {
        public InvalidSettledPriceException(string message, decimal invalidPrice)
      : base(string.Format(ConfigurationManager.AppSettings["exInvalidSettledPrice"], message), new PricingEngineException(invalidPrice.ToString()))
        {

        }
    }
}
