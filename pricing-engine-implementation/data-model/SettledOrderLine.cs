﻿using price_agreement.agreement;
using price_agreement.basic;
using pricing_engine.data_model;
using System;
using System.Collections.Generic;
using System.Text;

namespace pricing_engine_implementation
{
    public class SettledOrderLine : AbstractSettledOrderLine
    {
        public SettledOrderLine(IProduct product, IListPriceRule listPriceRule) : base(product, listPriceRule)
        {
        }

        public override AbstractSettledLineDetail NewLineDetail(IOrderLine orderLine)
        {
            return new SettledLineDetail(orderLine);
        }
    }
}