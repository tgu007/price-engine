﻿using price_agreement.agreement;
using price_agreement.basic;
using pricing_engine.data_model;

namespace pricing_engine_implementation
{
    public class SettledOrder : AbstractSettledOrder
    {
        public override AbstractSettledOrderLine NewLine(IProduct product, IListPriceRule listPriceRule)
        {
            return new SettledOrderLine(product, listPriceRule);
        }
    }
}
