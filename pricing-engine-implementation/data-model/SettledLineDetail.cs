﻿using price_agreement.agreement;
using pricing_engine.data_model;

namespace pricing_engine_implementation
{
    public class SettledLineDetail : AbstractSettledLineDetail
    {
        private IPriceAgreementBreakTrigger breakTrigger;
        public SettledLineDetail(IOrderLine scanedLineSnapshot) : base(scanedLineSnapshot)
        {
        }

        public IPriceAgreementBreakTrigger BreakTrigger {
            get
            {
                return breakTrigger;
            } 
            set { breakTrigger = value; } }
    }
}
