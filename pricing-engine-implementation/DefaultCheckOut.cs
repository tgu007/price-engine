﻿using price_agreement.agreement;
using pricing_engine.data_model;
using pricing_engine.service;
using System.Collections.Generic;

namespace pricing_engine_implementation
{
    public class DefaultCheckOut : AbstractCheckout
    {
        public DefaultCheckOut(List<IPriceAgreement> agreementList) : base(agreementList)
        {
        }

        public override AbstractSettledOrder NewSettlementOrder()
        {
            return new SettledOrder();
        }
    }
}
