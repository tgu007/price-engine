﻿using price_agreement.agreement;
using pricing_engine.data_model;
using pricing_engine.service.strategy;
using System;

namespace pricing_engine_implementation
{
    //When trigger met, apply certain discount on that order line.
    //EG buy 1 and get 1 half price.
    public class DollarOffStrategyManage : ICalculateDiscountStrategy
    {
        public void PerformCalculation(AbstractSettledOrderLine settledOrderLine)
        {
            AbstractSettledLineDetail lineDetail = settledOrderLine.GetLastLineDetail();
            IDiscountRule agreementRule = lineDetail.DiscountRuleApplied;
            lineDetail.AppliedDiscount = Math.Round(agreementRule.DiscountValue * lineDetail.ScanedLineSnapshot.Quantity, 2);
        }
    }
}
