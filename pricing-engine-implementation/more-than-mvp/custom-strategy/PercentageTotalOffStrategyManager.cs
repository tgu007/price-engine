﻿using price_agreement.agreement;
using pricing_engine.data_model;
using pricing_engine.service.strategy;
using System;
using System.Collections.Generic;

namespace pricing_engine_implementation
{
    //When trigger met, apply certain discount on that order line.
    //EG buy 1 and get 1 half price.
    public class PercentageTotalOffStrategyManager : ICalculateDiscountStrategy
    {
        public void PerformCalculation(AbstractSettledOrderLine settledOrderLine)
        {
            AbstractSettledLineDetail lineDetail = settledOrderLine.GetLastLineDetail();
            IDiscountRule agreementRule = lineDetail.DiscountRuleApplied;
            foreach(var lineDetail1 in settledOrderLine.LineDetailList)
            {
                lineDetail1.Value.AppliedDiscount = Math.Round(agreementRule.DiscountValue * settledOrderLine.ListPriceRuleApplied.ListPrice, 2);
            }
        }
    }
}
