﻿using price_agreement.agreement;
using pricing_engine.data_model;
using pricing_engine.service.strategy;
using System;
using System.Collections.Generic;
using System.Text;

namespace pricing_engine_implementation.more_than_mvp
{
    public class PriceBreakSteppedStrategyManager : ICalculateDiscountStrategy
    {
        //Triggers different discount on different level trigger quantity.
        //EG buy 1 and get 1 dollar off
        //Buy 2 and 2 dollar off
        //Buy 3 and get 3 dollar off.
        public void PerformCalculation(AbstractSettledOrderLine settledOrderLine)
        {
            SettledLineDetail lineDetail = (SettledLineDetail)settledOrderLine.GetLastLineDetail();
            lineDetail.AppliedDiscount = Math.Round(lineDetail.BreakTrigger.DiscountValue, 2);
        }
    }
}
