﻿using price_agreement.agreement;
using pricing_engine.data_model;
using pricing_engine.service;
using System;
using System.Collections.Generic;
using System.Text;

namespace pricing_engine_implementation
{
    public class QuantityCompareValidationManager:ValidationManager
    {
        public override bool IsTriggerConditionMet(AbstractSettledOrderLine settledLine, IDiscountRule agreementRule)
        {
            decimal totalSettledQuantity = settledLine.GetTotalSettledQuantity();
            return ( settledLine.GetTotalSettledQuantity() >= agreementRule.TriggerQuantity);
        }
    }
}
