﻿using price_agreement.agreement;
using pricing_engine.data_model;
using pricing_engine.service;
using System.Linq;

namespace pricing_engine_implementation
{
    public class PriceBreakSteppedValidationManager : ValidationManager
    {
        public override bool IsTriggerConditionMet(AbstractSettledOrderLine settledLine, IDiscountRule agreementRule)
        {
            if(agreementRule is IPriceAgreementBreakBasedRule)
            {
                IPriceAgreementBreakBasedRule breakBasedRule = (IPriceAgreementBreakBasedRule)agreementRule;
                //Find highest level trigger. 
                IPriceAgreementBreakTrigger breakTrigger = breakBasedRule.BreakTriggerList.
                    Where(trigger => settledLine.GetTotalSettledQuantity() >= trigger.TriggerQuantity)
                    .OrderBy(trigger => trigger.TriggerQuantity)
                    .LastOrDefault();
                //Set trigger to last scaned line
                //Should create a new interface or using generic type here, just cast for now
                ((SettledLineDetail)settledLine.GetLastLineDetail()).BreakTrigger = breakTrigger;
                return breakTrigger != null;
            }
            else
            {
                return false;
            }
        }
    }
}
