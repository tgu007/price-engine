using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using price_agreement.agreement;
using price_agreement.basic;
using pricing_engine.data_model;
using pricing_engine.service.strategy;
using pricing_engine_implementation;
using pricing_engine_implementation.more_than_mvp;
using System;
using System.Collections.Generic;

namespace check_out_test
{
    [TestClass]
    public class DefaultCheckoutTest
    {
        private class CheckoutPram
        {
            Random rand = new Random();
            public decimal listPrice;
            public decimal triggerQuantity;
            public decimal discount;
            public decimal lineQuantity;
            public int scanCounts;
            public CheckoutPram()
            {

                listPrice = RandomDecimal(100, 1000);
                triggerQuantity = rand.Next(1, 10);
                discount = RandomDecimal(1, 50);
                scanCounts = rand.Next(10, 100);
                lineQuantity = 1;
            }

            public decimal RandomDecimal(double minValue, double maxValue)
            {
                double randNumber = rand.NextDouble() * (maxValue - minValue) + minValue;
                return Convert.ToDecimal(Math.Round(randNumber, 2));
            }
        }


        [TestMethod]
        public void TestDefault()
        {
            //Generate Random Pram
            CheckoutPram pram = new CheckoutPram();
            //Mock Product 
            IProduct mockProduct = this.mockProduct();
            //Mock agreement
            IPriceAgreement mockAgreement = this.mockAgreement(pram.listPrice, pram.triggerQuantity, pram.discount, mockProduct);
            //Mock orderLine
            IOrderLine mockOrderLine = this.mockOrderLine(mockProduct, pram.lineQuantity);

            //Check out
            DefaultCheckOut defaultCheckOut = new DefaultCheckOut(new List<IPriceAgreement>() { mockAgreement });
            for (int i = 1; i <= pram.scanCounts; i++)
            {
                int discountedNumber = (int)(i * pram.lineQuantity / pram.triggerQuantity);
                decimal totalDiscounts = Math.Round(discountedNumber * pram.discount, 2);
                decimal totalListPrice = Math.Round(i * pram.listPrice * pram.lineQuantity, 2);
                this.ScanAndCheck(defaultCheckOut, mockOrderLine, totalListPrice - totalDiscounts);
            }

        }

        [TestMethod]
        //Buy 1 and get second half price.
        public void TestPercentageOff()
        {
            //Generate Random Pram
            CheckoutPram pram = new CheckoutPram();
            pram.discount = (decimal)0.5;
            pram.triggerQuantity = 2;
            //Mock Product 
            IProduct mockProduct = this.mockProduct();
            //Mock agreement

            IPriceAgreement mockAgreement = this.mockAgreement(pram.listPrice, pram.triggerQuantity, pram.discount, mockProduct);
            //Mock orderLine
            IOrderLine mockOrderLine = this.mockOrderLine(mockProduct, pram.lineQuantity);

            //Check out
            DefaultCheckOut defaultCheckOut = new DefaultCheckOut(new List<IPriceAgreement>() { mockAgreement });
            //replace the strategy manager
            PercentageOffStrategyManager strategyManager = new PercentageOffStrategyManager();
            defaultCheckOut.CalculationStrategyList = new SortedList<int, ICalculateDiscountStrategy>() { { 1, strategyManager } };
            for (int i = 1; i <= pram.scanCounts; i++)
            {
                int discountedNumber = (int)(i * pram.lineQuantity / pram.triggerQuantity);
                decimal totalDiscounts = Math.Round(pram.listPrice * pram.discount * discountedNumber, 2);
                decimal totalListPrice = Math.Round(i * pram.listPrice * pram.lineQuantity, 2);
                this.ScanAndCheck(defaultCheckOut, mockOrderLine, totalListPrice - totalDiscounts, false);
            }

        }

        [TestMethod]
        //Buy more than 3 then get 1 dollar off for each more purchased
        public void TestDollarOff()
        {
            //Generate Random Pram
            CheckoutPram pram = new CheckoutPram();
            pram.discount = (decimal)1;
            pram.triggerQuantity = 4;
            //Mock Product 
            IProduct mockProduct = this.mockProduct();
            //Mock agreement

            IPriceAgreement mockAgreement = this.mockAgreement(pram.listPrice, pram.triggerQuantity, pram.discount, mockProduct);
            //Mock orderLine
            IOrderLine mockOrderLine = this.mockOrderLine(mockProduct, pram.lineQuantity);

            //Check out
            DefaultCheckOut defaultCheckOut = new DefaultCheckOut(new List<IPriceAgreement>() { mockAgreement });
            //replace the strategy manager
            DollarOffStrategyManage strategyManage = new DollarOffStrategyManage();
            defaultCheckOut.CalculationStrategyList = new SortedList<int, ICalculateDiscountStrategy>() { { 1, strategyManage } };
            //replace the validation manager
            defaultCheckOut.ValidationManager = new QuantityCompareValidationManager();
            for (int i = 1; i <= pram.scanCounts; i++)
            {
                int discountedNumber = 0;
                if (i > 3)
                    discountedNumber = i - 3;
                decimal totalDiscounts = Math.Round(pram.discount * discountedNumber, 2);
                decimal totalListPrice = Math.Round(i * pram.listPrice * pram.lineQuantity, 2);
                this.ScanAndCheck(defaultCheckOut, mockOrderLine, totalListPrice - totalDiscounts);
            }

        }


        [TestMethod]
        //Buy 1 get 1 dollar off on line 1,
        //Buy 2 get 2 dollars off on line 2,
        //Buy 3 get 3 dollars off on line 3,
        public void TestSteppedBreak()
        {
            //Generate Random Pram
            CheckoutPram pram = new CheckoutPram();
            //Mock Product 
            IProduct mockProduct = this.mockProduct();
            //Mock agreement
            Mock<IPriceAgreementBreakBasedRule> breakBasedRule = this.mockBreakBasedRule(mockProduct);
            IPriceAgreement mockAgreement = this.mockAgreement(pram.listPrice, pram.triggerQuantity, pram.discount, mockProduct, breakBasedRule);

            //Mock orderLine
            IOrderLine mockOrderLine = this.mockOrderLine(mockProduct, pram.lineQuantity);

            //Check out
            DefaultCheckOut defaultCheckOut = new DefaultCheckOut(new List<IPriceAgreement>() { mockAgreement });
            //replace the strategy manager
            PriceBreakSteppedStrategyManager strategyManage = new PriceBreakSteppedStrategyManager();
            defaultCheckOut.CalculationStrategyList = new SortedList<int, ICalculateDiscountStrategy>() { { 1, strategyManage } };
            //replace the validation manager
            defaultCheckOut.ValidationManager = new PriceBreakSteppedValidationManager();
            for (int i = 1; i <= 4; i++)
            {
                decimal totalDiscounts = 0;

                switch (i)
                {
                    case 1:
                        totalDiscounts = 1;
                        break;
                    case 2:
                        totalDiscounts = 1 + 2;
                        break;
                    case 3:
                        totalDiscounts = 1 + 2 + 3;
                        break;
                    case 4: //The highest trigger is 3 so on fouth itme still get 3 dollars discount.
                        totalDiscounts = 1 + 2 + 3 + 3;
                        break;

                }
                decimal totalListPrice = Math.Round(i * pram.listPrice * pram.lineQuantity, 2);
                this.ScanAndCheck(defaultCheckOut, mockOrderLine, totalListPrice - totalDiscounts);
            }

        }

        private Mock<IPriceAgreementBreakBasedRule> mockBreakBasedRule(IProduct mockProduct)
        {
            Mock<IPriceAgreementBreakBasedRule> breakBasedRule = new Mock<IPriceAgreementBreakBasedRule>();
            breakBasedRule.SetupGet(rule => rule.AgreementProduct).Returns(mockProduct);
            List<IPriceAgreementBreakTrigger> triggerList = new List<IPriceAgreementBreakTrigger>();
            triggerList.Add(this.mockBreakTrigger(1, 1));
            triggerList.Add(this.mockBreakTrigger(2, 2));
            triggerList.Add(this.mockBreakTrigger(3, 3));
            breakBasedRule.SetupGet(rule => rule.BreakTriggerList).Returns(triggerList);
            return breakBasedRule;
        }

        private void ScanAndCheck(DefaultCheckOut defaultCheckOut, IOrderLine mockOrderLine, decimal expectedTotal)
        {
            this.ScanAndCheck(defaultCheckOut, mockOrderLine, expectedTotal, true);
        }
        private void ScanAndCheck(DefaultCheckOut defaultCheckOut, IOrderLine mockOrderLine, decimal expectedTotal, bool compareIntPartOnly)
        {
            defaultCheckOut.Scan(mockOrderLine);
            decimal totalPrice = defaultCheckOut.SettledOrder.getTotalPrice();
            //Some rouding issue, just bypass for now.
            if (compareIntPartOnly)
                Assert.IsTrue((int)totalPrice == (int)expectedTotal);
            else
                Assert.IsTrue(totalPrice == expectedTotal);
        }

        private IOrderLine mockOrderLine(IProduct mockProduct, decimal lineQuantity)
        {
            //Mock Order Line
            Mock<IOrderLine> mockOrderLine = new Mock<IOrderLine>();
            mockOrderLine.SetupGet(ol => ol.Quantity).Returns(lineQuantity);
            mockOrderLine.SetupGet(ol => ol.Product).Returns(mockProduct);
            return mockOrderLine.Object;
        }

        private IProduct mockProduct()
        {
            Mock<IProduct> mockProduct = new Mock<IProduct>();
            mockProduct.SetupGet(product => product.Id).Returns(new System.Guid());
            mockProduct.SetupGet(product => product.ResolvedProductList).Returns(new List<IProduct>() { mockProduct.Object });
            return mockProduct.Object;
        }

        private IPriceAgreement mockAgreement(decimal listPrice, decimal triggerQuantity, decimal discountValue, IProduct mockProduct)
        {
            return mockAgreement(listPrice, triggerQuantity, discountValue, mockProduct, null);
        }

        private IPriceAgreement mockAgreement(decimal listPrice, decimal triggerQuantity, decimal discountValue, IProduct mockProduct, Mock<IPriceAgreementBreakBasedRule> breakBasedRule)
        {
            Mock<IListPriceRule> listPriceRule = new Mock<IListPriceRule>();
            listPriceRule.SetupGet(rule => rule.AgreementProduct).Returns(mockProduct);
            listPriceRule.SetupGet(rule => rule.ListPrice).Returns(listPrice);

            Mock<IPriceAgreement> mockAgreement = new Mock<IPriceAgreement>();
            mockAgreement.SetupGet(agreement => agreement.AgreementListPriceRuleList).Returns((new List<IListPriceRule>() { listPriceRule.Object }));

            if (breakBasedRule != null)
                mockAgreement.SetupGet(agreement => agreement.AgreementDiscountRuleList).Returns((new List<IDiscountRule>() { breakBasedRule.Object }));
            else
            {
                Mock<IDiscountRule> discountRule = new Mock<IDiscountRule>();
                discountRule.SetupGet(rule => rule.AgreementProduct).Returns(mockProduct);
                discountRule.SetupGet(rule => rule.TriggerQuantity).Returns(triggerQuantity);
                discountRule.SetupGet(rule => rule.DiscountValue).Returns(discountValue);
                mockAgreement.SetupGet(agreement => agreement.AgreementDiscountRuleList).Returns((new List<IDiscountRule>() { discountRule.Object }));
            }
            return mockAgreement.Object;
        }

        private IPriceAgreementBreakTrigger mockBreakTrigger(decimal triggerQuantity, decimal discount)
        {
            Mock<IPriceAgreementBreakTrigger> breakTrigger = new Mock<IPriceAgreementBreakTrigger>();
            breakTrigger.Setup(trigger => trigger.TriggerQuantity).Returns(triggerQuantity);
            breakTrigger.Setup(trigger => trigger.DiscountValue).Returns(discount);
            return breakTrigger.Object;
        }
    }
}
