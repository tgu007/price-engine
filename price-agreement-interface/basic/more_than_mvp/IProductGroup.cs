﻿
using System.Collections.Generic;

namespace price_agreement.basic
{
    public interface IProductGroup : IAgreementProduct
    {
        public List<IProduct> ProductList { get; }

        List<IProduct> IAgreementProduct.ResolvedProductList { get => ProductList; }
    }
}
