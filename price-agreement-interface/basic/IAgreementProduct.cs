﻿using System.Collections.Generic;

namespace price_agreement.basic
{
    //The product interace been set on Iagreement
    //Puporse to have this interface is to support when mulitple products valid for one rule.
    //Instead of add multiple rule records for each product, put the product into a group or hierarchy and set that single entity to agreement.
    public interface IAgreementProduct
    {
        List<IProduct> ResolvedProductList { get; }
    }
}
