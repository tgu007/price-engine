﻿using System;
using System.Collections.Generic;

namespace price_agreement.basic
{
    public interface IProduct : IAgreementProduct
    {
        public Guid Id { get; }

        List<IProduct> IAgreementProduct.ResolvedProductList { get=>new List<IProduct>() { this}; }
    }
}
