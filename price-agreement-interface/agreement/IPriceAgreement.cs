﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace price_agreement.agreement
{
    //Puporse fo this interface is for future extension on common behaviour applied to rules 
    //eg, multiple rules need to apply on a list of stores 
    //Just need to create a new interface for IStorePriceAgreement extends this one and contains a property of "ValidStores{get;}"
    public interface IPriceAgreement
    {
        public IEnumerable<IListPriceRule> AgreementListPriceRuleList { get;}

        public IEnumerable<IDiscountRule> AgreementDiscountRuleList { get; }

    }
}
