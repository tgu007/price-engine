﻿
using System.ComponentModel.DataAnnotations;

namespace price_agreement.agreement
{
    public interface IDiscountRule:IPriceAgreementRule
    {
        [Range(0.01, double.MaxValue, ErrorMessage = "Trigger Quantity must greater than 0")]
        public decimal TriggerQuantity { get; }

        [Range(0.01, double.MaxValue, ErrorMessage = "Discount value must greater than 0")]
        public decimal DiscountValue { get; }
    }
}
