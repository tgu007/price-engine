﻿using System.ComponentModel.DataAnnotations;

namespace price_agreement.agreement
{
    public interface IListPriceRule:IPriceAgreementRule
    {
        [Range(0.01, double.MaxValue, ErrorMessage = "List price must greater than 0")]
        public decimal ListPrice { get; }
    }
}
