﻿
using price_agreement.basic;
using System;

namespace price_agreement.agreement
{
    //Split into list price rule and discount rule support future extnsion
    //Eg: Product A can have different list price under some condition
    // When apply to list price rule 'A', apply discount Rule 'B'
    // When apply to list price rule 'C', apply discoount rule 'D'
    public interface IPriceAgreementRule

    {
        IAgreementProduct AgreementProduct { get; }

        DateTime? StartDate { get => null; }

        DateTime? EndDate { get => null; }
    }
}
