﻿
using System.Collections.Generic;

namespace price_agreement.agreement
{
    //A break based rule supports multiple level discounts
    //It will contains a list of triggers
    //An example of trigger looks like below
    //Buy 1 got no discount
    //Buy 2 get 1 dollar discount
    //Buy 3 got 2 dollar discount

    public interface IPriceAgreementBreakBasedRule : IDiscountRule
    {
        public IEnumerable<IPriceAgreementBreakTrigger> BreakTriggerList { get; }
    }
}
