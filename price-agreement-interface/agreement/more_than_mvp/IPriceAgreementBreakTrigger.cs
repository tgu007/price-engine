﻿
using System.ComponentModel.DataAnnotations;

namespace price_agreement.agreement
{
    //This interface specifies a trigger condition and a rewardvalue when the condition has been met.
    public interface IPriceAgreementBreakTrigger
    {
        [Range(0.01, double.MaxValue, ErrorMessage = "Trigger Quantity must greater than 0")]
        public decimal TriggerQuantity { get; }

        [Range(0.01, double.MaxValue, ErrorMessage = "Discount value must greater than 0")]
        public decimal DiscountValue { get;}

    }
}
