﻿using System;

namespace extension_sdk
{
    public static class DateTimeExtension
    {
        public static bool IsBetween(this DateTime dateTime, DateTime? startDate, DateTime? endDate)
        {
            //datetime before start date
            if (startDate != null && DateTime.Compare(dateTime, startDate.Value) < 0)
                return false;

            //datetime after end date
            if (endDate != null && DateTime.Compare(dateTime, startDate.Value) > 0)
                return false;

            return true;
        }
    }
}
