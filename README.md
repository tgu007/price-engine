## requirement of this implementation
http://codekata.com/kata/kata09-back-to-the-checkout/
   
## Solution Structure
--price-agreement-interface:
  Define the data structure of the price agreement, which is the "rule" from the requirement. 

--pricing-engine-interface:
  Define the abstract data model of "settled order" which is a collection of scanned line results.
  "Settled Order line" contains information including unit price, discount applied, the rule applied and origin scanned line snapshot.
  
  Define the abstract data model of the expected scan line.
  
  Define abstract service logic for how a checkout service should work with "rules" from price-agreement-interface
  
  Provide a default implementation according to the example from the requirement link above.

  Design Pattern: 
  Strategy pattern used to handle future validation and calculation rule changes. 
  This pattern gives the ability to scale horizontally once the rule is changed or added.

--pricing-engine-implementation 
  An implementation of pricing-engine-interface.
  Include some possible future requirement changes implementation inside more-than-mvp folder. See Implemented Scenarios Section 

--check-out-test
  Test project for pricing-engine-implementation 

--extension-sdk
  Common SDK Library.

## Implemented Scenarios
1: Buy more than X units and get a Y dollars discount on the total order. Default implementation from requirement link. 

2: Buy more than X units and get Y percentage off on Zth purchased units. EG buys 2 and pays half for the second.

3: Buy more than X units and start to get a Y dollars unit discount on each unit purchased onwards.

4: Stepped Discount
   Eg: Buy 1 to 10 units and get 1 dollar discount in this range
       Buy 10 to 20 units and get 2 dollars discount in this range
       Buy 20+ more units and get 3 dollars discount

## How to handle new scenarios
Based on the requirement, add a new calculation strategy class or validation class. 

Inherited appropriate interface to support the calculation logic. 

See examples inside the MVP folder from pricing-engine-implementation 

## Road Map
1: Implement price-agreement-interface and use the database to store rules.

2: Add a restful service layer to consume this library.
   Provide check-out as a GET API.
   Load all rules into a cache like elastic cache.

3: Add front end for rule maintenance.

4: Add front end to call restful service from step 2.
